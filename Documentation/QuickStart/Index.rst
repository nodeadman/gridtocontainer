
.. include:: /Includes.rst.txt

.. _quick-start:

===========
Quick start
===========

We assume you have composer-based TYPO3 installation with extension
EXT:gridelments (:bash:`gridelementsteam/gridelements`) installed.

.. rst-class:: bignums

#. Make Backups or work on a development system

   .. warning::
      Create a backup of your database and filesystem **before** applying these
      steps. It is highly recommended to use this extension on a
      **development system only**.

#. Install EXT:container

   .. code-block:: bash

      composer req b13/container

   Keep EXT:gridelements installed for now.

#. Install EXT:gridtocontainer

   .. code-block:: bash

      composer req --dev sbublies/gridtocontainer

   See the chapter on :ref:`installation`. Make sure the TypoScript template is
   included. We install the extension as a dev-requirement as it should not be
   used on productive systems.

#. Check which of the available grid elements are in use

   Go to the module :guilabel:`Admin tools > Grid migration` provided by
   this extension. See also :ref:`grid-elements-in-use`.

#. Create alternative containers for each grid element

   See the `EXT:container documentation <https://github.com/b13/container>`__.

#. Migrate all grid elements to containers

   Using the module :guilabel:`Admin tools > Grid migration` migrate all grid
   elements to the respective containers. See also :ref:`migrate_from_gridelements`.

#. Optional: Migrate FlexForm content

   If your grid elements used FlexForms you have to migrate the values stored
   in the FlexForms by other means. See chapter :ref:`known-problems`.

#. Check the migration

   Is everything being displayed as expected in frontend and backend?

#. Remove EXT:gridelements and this extension

   Remove or replace all extensions that require EXT:gridelements and remove the
   requirement from your sitepackage. Then remove EXT:gridelements. You can
   also remove EXT:gridtocontainer after successfully migrating all containers:

   .. code-block:: bash

      composer remove gridelementsteam/gridelements sbublies/gridtocontainer

#. Clean up the database

   Search in the database for content elements that still use the :sql:`colpos`
   `-1` and resolve them. See chapter :ref:`clean-up` for details.

#. Make a database compare

   Got to :guilabel:`Admin tools > Maintenance > Analyze Database` and
   undo all changes that EXT:gridelements made to your database.
